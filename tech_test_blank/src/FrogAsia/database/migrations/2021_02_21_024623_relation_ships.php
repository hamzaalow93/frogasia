<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelationShips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("frogs", function (Blueprint $table) {
            $table->foreign('pond_id')->references('id')->on('ponds');
        });

        Schema::table("frogs_mating", function (Blueprint $table) {
            $table->foreign('male_frog_id')->references('id')->on('frogs');
            $table->foreign('female_frog_id')->references('id')->on('frogs');
        });

        Schema::table("frog_notes", function (Blueprint $table) {
            $table->foreign('frog_id')->references('id')->on('frogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
