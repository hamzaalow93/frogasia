<?php

namespace Database\Seeders;

use App\Models\Pond;
use Illuminate\Database\Seeder;

class PondSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//      First Pond
        $firstPond=new Pond();
        $firstPond->name="first pond";
        $firstPond->space=212.5;
        $firstPond->environment="cold";
        $firstPond->save();
//      Second Pond
        $secondPond=new Pond();
        $secondPond->name="second pond";
        $secondPond->space=50;
        $secondPond->environment="hot";
        $secondPond->save();
    }
}
