<?php

namespace Database\Seeders;

use App\Models\Frog;
use App\Models\FrogNote;
use App\Models\Pond;
use Illuminate\Database\Seeder;

class FrogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        First Pond life
        /////////////////// create the frogs ///////////////////////
        $firstPond = Pond::find(1);
        $firstFrog = new Frog();
        $firstFrog->name = "First Frog";
        $firstFrog->gender = "male";
        $firstFrog->birth_date = "2020-12-05";

        $secondFrog = new Frog();
        $secondFrog->name = "Second Frog";
        $secondFrog->gender = "male";
        $secondFrog->birth_date = "2020-11-05";

        $thirdFrog = new Frog();
        $thirdFrog->name = "Third Frog";
        $thirdFrog->gender = "female";
        $thirdFrog->birth_date = "2021-01-01";
        $firstPond->frogs()->saveMany([
            $firstFrog,
            $secondFrog,
            $thirdFrog
        ]);
        ////////////////////// create the mating ////////////////////
        $firstFrog->frogsMating()->attach($thirdFrog);
        $secondFrog->frogsMating()->attach($thirdFrog);
        $firstFrog->frogsMating()->attach($thirdFrog);
        $secondFrog->frogsMating()->attach($thirdFrog);
        $firstFrog->frogsMating()->attach($thirdFrog);
        //////////////////// create the notes /////////////////////
        $firstFrog->notes()->saveMany([
            new FrogNote(["content" => "first note for the first frog"]),
            new FrogNote(["content" => "second note for the first frog"]),
            new FrogNote(["content" => "third note for the first frog"])
        ]);

        $secondFrog->notes()->saveMany([
            new FrogNote(["content" => "first note for the second frog"]),
            new FrogNote(["content" => "second note for the second frog"]),
            new FrogNote(["content" => "third note for the second frog"])
        ]);

//      Second Pond life
        $secondPond = Pond::find(2);
        /////////////////// create the frogs ///////////////////////
        $fourthFrog = new Frog();
        $fourthFrog->name = "Fourth Frog";
        $fourthFrog->gender = "male";
        $fourthFrog->birth_date = "2020-12-07";

        $fifthFrog = new Frog();
        $fifthFrog->name = "fifth Frog";
        $fifthFrog->gender = "female";
        $fifthFrog->birth_date = "2020-09-09";

        $sixthFrog = new Frog();
        $sixthFrog->name = "sixth Frog";
        $sixthFrog->gender = "female";
        $sixthFrog->birth_date = "2021-11-09";
        $secondPond->frogs()->saveMany([
            $fourthFrog,
            $fifthFrog,
            $sixthFrog
        ]);
        ////////////////////// create the mating ////////////////////
        $sixthFrog->frogsMating()->attach($fourthFrog);
        $fifthFrog->frogsMating()->attach($fourthFrog);
        $fifthFrog->frogsMating()->attach($fourthFrog);
        $fifthFrog->frogsMating()->attach($fourthFrog);
        $sixthFrog->frogsMating()->attach($fourthFrog);
        //////////////////// create the notes /////////////////////
        $fourthFrog->notes()->saveMany([
            new FrogNote(["content" => "first note for the fourth frog"]),
            new FrogNote(["content" => "second note for the fourth frog"]),
            new FrogNote(["content" => "third note for the fourth frog"])
        ]);

        $fifthFrog->notes()->saveMany([
            new FrogNote(["content" => "first note for the fifth frog"]),
            new FrogNote(["content" => "second note for the fifth frog"]),
            new FrogNote(["content" => "third note for the fifth frog"])
        ]);
    }
}
