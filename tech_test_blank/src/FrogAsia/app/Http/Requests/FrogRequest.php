<?php

namespace App\Http\Requests;


use Illuminate\Validation\Rule;

class FrogRequest extends ApplicationRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>"required|string",
            "gender"=>[
                "required",
                Rule::in(["male","female"])
            ],
            "birth_date"=>"required|date"
        ];
    }
}
