<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FrogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "gender" => $this->gender,
            "birth_date" => $this->birth_date,
            "death_date" => $this->death_date,
            "created_at" => $this->created_at,
            "pond" => $this->pond,
            "notes" => $this->notes,
            "mating"=>$this->frogsMating
        ];
    }
}
