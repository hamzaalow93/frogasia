<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class FrogCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "gender"=>$this->gender,
            "birth_date"=>$this->birth_date,
            "death_date"=>$this->death_date,
            "created_at"=>$this->created_at,
            "pond"=>$this->pond
        ];
    }
}
