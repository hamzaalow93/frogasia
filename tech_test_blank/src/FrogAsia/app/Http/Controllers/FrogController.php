<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrogRequest;
use App\Http\Resources\FrogCollection;
use App\Http\Resources\FrogResource;
use App\Models\Frog;
use App\Models\Pond;
use Illuminate\Http\Request;

class FrogController extends Controller
{
    public function index(Pond $pond)
    {
        return response(
            FrogCollection::collection($pond->frogs)
        );
    }


    public function store(FrogRequest $request,Pond $pond)
    {
        $frog=new Frog();
        $frog->name=$request->name;
        $frog->gender=$request->gender;
        $frog->birth_date=$request->birth_date;
        $pond->frogs()->save($frog);
        return response(
            new FrogCollection($frog),
            201
        );
    }


    public function show(Pond $pond,Frog $frog)
    {
        return response(
            new FrogResource($frog)
        );
    }


    public function update(FrogRequest $request,Pond $pond,Frog $frog)
    {
        $frog->name=$request->name;
        $frog->gender=$request->gender;
        $frog->birth_date=$request->birth_date;
        $pond->frogs()->save($frog);
        return response(
            new FrogResource($frog)
        );
    }

    public function destroy(Pond $pond,Frog $frog)
    {
        $frog->delete();
        return response(
            "Deleted successfully"
        );
    }

    public function die(Request $request,Frog $frog){
        $request->validate([
            'death_date' => "required|date",
        ]);
        $frog->death_date=$request->death_date;
        return response(
            new FrogResource($frog)
        );
    }
    public function mate(Frog $first_frog,Frog $second_frog){
        $first_frog->frogsMating()->attach($second_frog);
        return response(
          "congratulations ^_^"
        );
    }
}
