<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrogNoteRequest;
use App\Http\Resources\FrogNoteCollection;
use App\Http\Resources\FrogNoteResource;
use App\Models\Frog;
use App\Models\FrogNote;
use Illuminate\Http\Request;

class FrogNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Frog $frog)
    {
        return response(
            FrogNoteCollection::collection($frog->notes)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FrogNoteRequest $request,Frog $frog)
    {
        $frogNote=new FrogNote();
        $frogNote->content=$request->content;
        $frog->notes()->save($frogNote);
        return response(
            new FrogNoteResource($frogNote),
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Frog $frog,FrogNote $note)
    {
        return response(
            new FrogNoteResource($note)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FrogNoteRequest $request,Frog $frog,FrogNote $note)
    {
        $note->content=$request->content;
        $frog->notes()->save($note);
        return response(
            new FrogNoteResource($note)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Frog $frog,FrogNote $note)
    {
        $note->delete();
        return response(
            "Deleted successfully"
        );
    }
}
