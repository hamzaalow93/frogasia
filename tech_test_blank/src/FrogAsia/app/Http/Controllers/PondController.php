<?php

namespace App\Http\Controllers;

use App\Http\Requests\PondRequest;
use App\Http\Resources\PondCollection;
use App\Http\Resources\PondResource;
use App\Models\Pond;
use Illuminate\Http\Request;

class PondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ponds=Pond::all();
        return response(PondCollection::collection($ponds));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PondRequest $request)
    {
        $pond=new Pond();
        $pond->name=$request->name;
        $pond->space=$request->space;
        $pond->environment=$request->environment;
        $pond->save();
        return response(
            new PondResource($pond),
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pond $pond)
    {
        return response(
            new PondResource($pond)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PondRequest $request, Pond $pond)
    {
        $pond->name=$request->name;
        $pond->space=$request->space;
        $pond->environment=$request->environment;
        $pond->save();
        return response(
            new PondResource($pond)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pond $pond)
    {
        $pond->delete();
        return response(
            "Deleted successfully"
        );
    }
}
