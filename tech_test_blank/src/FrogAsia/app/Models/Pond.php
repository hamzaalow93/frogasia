<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pond extends Model
{
    use HasFactory;

    public function frogs(){
        return $this->hasMany(Frog::class,"pond_id","id");
    }

}
