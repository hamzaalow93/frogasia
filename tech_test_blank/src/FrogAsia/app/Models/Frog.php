<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Frog extends Model
{
    use HasFactory;

    public function pond(){
        return $this->belongsTo(Pond::class,"pond_id","id");
    }
    public function notes(){
        return $this->hasMany(FrogNote::class,"frog_id","id");
    }
    public function frogsMating(){
        if($this->gender=="male"){
            return $this->belongsToMany(Frog::class,
                "frogs_mating","male_frog_id","female_frog_id");

        }else{
            return $this->belongsToMany(Frog::class,
                "frogs_mating","female_frog_id","male_frog_id");
        }
    }
}
