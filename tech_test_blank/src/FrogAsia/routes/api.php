<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource("ponds",Controllers\PondController::class);
Route::apiResource("ponds.frogs",Controllers\FrogController::class);
Route::post("frogs/{frog}/die",[Controllers\FrogController::class,"die"]);
Route::post("frogs/mate/{first_frog}/{second_frog}",[Controllers\FrogController::class,"mate"]);
Route::apiResource("frogs.notes",Controllers\FrogNoteController::class);
