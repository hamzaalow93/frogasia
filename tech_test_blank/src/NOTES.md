# Prerequisites
### Download and install the git package via:

```bash
https://git-scm.com/
```

### Download and install the composer package via:

```bash
https://getcomposer.org/
```

# Clone the project

Clone the project via git:
```bash
git clone  https://hamzaalow93@bitbucket.org/hamzaalow93/frogasia.git
```

# Installation

- Open the command Prompt inside the downloaded project.
- Execute the following command to download the vendor libraries:
```bash
cd tech_test_blank/src/FrogAsia
composer install
```
- create a database with the name "frogasia"
  
- Execute the command:
```bash
php artisan migrate:fresh --seed
```
>> the database seeders are located in "database/seeders"

# Database ERD

![alt text](https://bitbucket.org/hamzaalow93/frogasia/raw/66d7d032f0e350616eee8e9742537ced36b780c5/tech_test_blank/src/FrogAsia/ERD.PNG)

# FrogAsia Task

- run the server
```bash
php artisan serve
```

- access to the api documentation (swagger)
```bash
http://127.0.0.1:8000/api/documentation
```

- 